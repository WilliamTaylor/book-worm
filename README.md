![alt text](https://book-worm.willtaylor.info/media/icons/android-chrome-192x192.png "Bookworm Logo")

# Bookworm

## A [Progressive webapp](https://developers.google.com/web/progressive-web-apps/) written in [Vue.js]

Live version found at: https://book-worm.willtaylor.info

This application has been tested working across Chrome, Firefox and Chrome for Android. Other browsers may not behave as expected. 

BookWorm is a progressive web app that provides an interface for 
[Google Books V1 REST API](https://developers.google.com/books/) 
that provides an easy interface to search for books provided by the API.

Technologies Used:
* [Service Worker]
* [IndexedDB]
* [Cache]
* [Vue.js]
* [Vue Async Computed]
* [Vue Router]
* [Sass]
* [Grunt]
* [Babel]
* [Webpack]
* [node.js]
* [npm]

### Features
#### Offline Viewing
When the web app is first visited, a [Service Worker] is installed on the device that listens for fetch events.
When a fetch event is called, the service worker intercepts it and checks to see if the requested resource is in the [Cache].
If the resource is found in the [Cache], it returns the saved resource, otherwise the fetch event is requested as usual.
 
By default, when the [Service Worker] is installed, the required files for running the application are cached so that
the application can launch without an internet connection. The code for the service worker can be found [here](https://gitlab.com/WilliamTaylor/book-worm/blob/master/serviceWorker.js).

#### Bookmarks
Alongside the ability to such for books, BookWorm also lets you bookmark a book so that 
you can look at it from the bookmarks page. Bookmarked books are available to you offline so
you can view the books image and description without an internet connection.

Once a book is bookmarked, all of its data is saved to an entry in the bookmarks store in
[IndexedDB] and the thumbnail and preview image are stored to the offline [Cache]

#### History
Books that you've previously visited in the app also available offline and are saved to the 
history store of the [IndexedDB] database, the thumbnail and preview image are also stored in the [Cache].

### Build Process Explained
#### Javascript
BookWorm is written in ES6 and should be compatible with any ES6 enabled browser. Unfortunately no browser yet is
fully ES6 compliant. This means that in order to make the application run in current browsers, the ES6 needs to be
compiled into ES5. This is where [Webpack] and [Babel] are used.

[Webpack] is a javascript build tool that let's you use ES6 module syntax to include many files and generates a single
javascipt file, known as a bundle, that you can explicitly add to your html pages. [Webpack] takes plugins that 
transform your files before bundling them. This project uses two plugins, [babel-loader] which uses [Babel] to compile to
ES5 and [vue-loader] that transforms [Vue.js] templates into plain Javascript.

#### Styles
Bookworms stylesheets are written in [Sass] (*.scss). [Sass] gives the ability to add variables and other features
like mixins to your stylesheets. [Grunt] takes these *.scss files and turns them into a singe CSS file.

#### Building the project yourself
Setting up this project on your machine is very easy.
1. Make sure you have the latest stable release of [node.js] and [npm] installed
2. Clone this repository
3. Install dependencies with `npm install`
3. Build
  * Run `npm run build` to run a script that builds the JS and CSS once only.
  * Run `npm run dev` to run a script that builds the JS and CSS and watches for changes to the files and rebuilds accordingly, this also runs a dev server and opens the webpage in a new tab. 



[Service Worker]: https://developer.mozilla.org/en/docs/Web/API/Service_Worker_API
[IndexedDB]: https://developer.mozilla.org/en/docs/Web/API/IndexedDB_API
[Cache]: https://developer.mozilla.org/en-US/docs/Web/API/Cache
[Vue.js]: https://vuejs.org/
[Vue Async Computed]: https://github.com/foxbenjaminfox/vue-async-computed
[Vue Router]: https://github.com/vuejs/vue-router
[Sass]: http://sass-lang.com/
[Grunt]: https://gruntjs.com/
[Babel]: https://babeljs.io/
[babel-loader]: https://github.com/babel/babel-loader
[vue-loader]: https://github.com/vuejs/vue-loader
[Webpack]: https://webpack.js.org/
[node.js]: https://nodejs.org/en/
[npm]: https://www.npmjs.com/
