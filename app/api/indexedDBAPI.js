const name = 'book_worm';
let db;

let request = indexedDB.open(name, 1);

request.onerror = function (event) {
	console.log(event.target.errorCode)
};

request.onupgradeneeded = function (event) {
	db = event.target.result;
	db.createObjectStore('bookmarks', {keyPath: 'id'});
	db.createObjectStore('history', {keyPath: 'id'});
};

request.onsuccess = function (event) {
	db = event.target.result;
};

function getDb() {
	return new Promise(function (resolve, reject) {
		if (db) resolve(db);
		else {
			request.addEventListener('success', function (event) {
				resolve(event.target.result);
			})
		}
	});
}

export default {

	addBookmark(volume) {

		return getDb().then(function () {
			return new Promise(function (resolve, reject) {
				let transaction = db.transaction(['bookmarks'], 'readwrite');

				transaction.oncomplete = resolve;
				transaction.onerror = reject;

				let bookmarkStore = transaction.objectStore('bookmarks');

				bookmarkStore.put(volume)
			})
		});



	},

	removeBookmark(volumeId) {

		return getDb().then(function () {
			return new Promise(function (resolve, reject) {
				let transaction = db.transaction(['bookmarks'], 'readwrite');

				transaction.oncomplete = resolve;
				transaction.onerror = reject;

				let bookmarkStore = transaction.objectStore('bookmarks');

				bookmarkStore.delete(volumeId)
			})
		});


	},

	isBookmarked(volumeId) {

		return getDb().then(function() {
			return new Promise(function (resolve, reject) {
				let transaction = db.transaction(['bookmarks'], 'readwrite');
				let bookmarkStore = transaction.objectStore('bookmarks');
				let request = bookmarkStore.get(volumeId);

				transaction.oncomplete = function (event) {
					resolve(!!request.result)
				};

				transaction.onerror = function (event) {
					reject(event)
				};
			});

		});

	},

	getBookmarks() {

		return getDb().then(function () {
			return new Promise(function (resolve, reject) {
				let transaction = db.transaction(['bookmarks'], 'readonly');
				let bookmarksStore = transaction.objectStore('bookmarks');

				let request = bookmarksStore.getAll()

				transaction.oncomplete = () => resolve(request.result);
				transaction.onerror = event => reject(event);

			})
		})
	},

	getBookmarkedVolume(volumeId) {
		return getDb().then(function () {
			return new Promise(function (resolve, reject) {
				let transaction = db.transaction(['bookmarks'], 'readonly');
				let bookmarksStore = transaction.objectStore('bookmarks');
				let request = bookmarksStore.get(volumeId);

				transaction.oncomplete = (event) => resolve(request.result);
				transaction.onerror = (event) => reject(event);

			})
		})
	},

	addToHistory(volume) {
		return getDb().then(function () {
			return new Promise(function (resolve, reject) {
				let transaction = db.transaction(['history'], 'readwrite');
				let historyStore = transaction.objectStore('history');
				let request = historyStore.put(volume);

				request.onsuccess = resolve;
				request.onerror = reject;

			})
		})
	},

	getVolumeFromHistory(volumeId) {
		return getDb().then(function () {
			return new Promise(function (resolve, reject) {
				let transaction = db.transaction(['history'], 'readonly');
				let historyStore = transaction.objectStore('history');
				let request = historyStore.get(volumeId);

				transaction.oncomplete = function (event) {
					resolve(request.result);
				};

				transaction.onerror = function (event) {
					reject(event);
				}
			})
		})
	},

	getHistory() {
		return getDb().then(function () {
			return new Promise(function (resolve, reject) {
				let transaction = db.transaction(['history'], 'readonly');
				let historyStore = transaction.objectStore('history');
				let request = historyStore.getAll();

				transaction.oncomplete = function (event) {
					resolve(request.result);
				};

				transaction.onerror = function (event) {
					reject(event);
				}
			})
		})
	}

}