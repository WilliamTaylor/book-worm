const cacheName = 'v2';

export default {

	add(url, corsDisabled) {

		const cachePromise = caches.open(cacheName);

		if (!corsDisabled) return cachePromise.then(cache => cache.add(url));

		else {
			const request = new Request(url, {mode: 'no-cors'});
			const response = fetch(request);

			return cachePromise.then(cache => response.then(r => {
				return cache.put(request, r);
			}))
		}
	},

	addAll(urls) {
		return caches.open(cacheName).then(cache => cache.addAll(urls))
	},

	remove(url) {
		return caches.open(cacheName).then(cache => cache.delete(url))
	}

}
