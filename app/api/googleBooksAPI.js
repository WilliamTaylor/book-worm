const volumesURL = 'https://www.googleapis.com/books/v1/volumes';

function GETRequest(url) {

	return new Promise(function (resolve, reject) {
		let xhr = new XMLHttpRequest();
		xhr.onload = function () {

			if (xhr.status === 200) {
				resolve(xhr.responseText);
			}

			else {
				reject({status: this.status, statusText: xhr.statusText});
			}

		};
		xhr.onerror = reject;
		xhr.open('GET', url);
		xhr.send();
	});


}

export default {

	query(searchQuery) {
		return GETRequest(volumesURL + '?q=' + searchQuery.replace(/ /g, '+')).then(JSON.parse)
			.then(results => results.items)
			.then(results => {
				if (results) {
					return results.filter(volume => volume.volumeInfo.imageLinks)
						.map(volume => {
							volume.thumbnail = volume.volumeInfo.imageLinks.smallThumbnail.replace('http', 'https');
							return volume
						});
				}
				return [];
			});
	},

	getVolume(id) {
		return GETRequest(volumesURL + '/' + id)
			.then(JSON.parse)
			.then(volume => {
				volume.coverImage = (volume.volumeInfo.imageLinks.large || volume.volumeInfo.imageLinks.thumbnail).replace('http', 'https');
				volume.thumbnail = volume.volumeInfo.imageLinks.smallThumbnail.replace('http', 'https');
				return volume;
			});
	}

}
