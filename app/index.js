import Vue from 'vue';
import app from './components/App.vue';
import router from './router';
import AsyncComputed from 'vue-async-computed';

Vue.use(AsyncComputed);

if ('serviceWorker' in navigator) {
	navigator.serviceWorker.register('./serviceWorker.js')
		.then(r => console.log('Registered Service worker', r)).catch(e => console.warn('Error registering service worker', e));
}

new Vue({
	el: '#appContainer',
	router,
	components: { app }
});