import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from './components/Home.vue';
import Search from './components/Search.vue';
import Volume from './components/Volume.vue';
import Bookmarks from './components/Bookmarks.vue';
import History from './components/History.vue';

Vue.use(VueRouter);

let router = new VueRouter({

	routes: [
		{name: 'home', path: '/', component: Home},
		{name: 'search', path: '/search/:query', component: Search},
		{name: 'emptySearch', path: '/search', component: Search},
		{name: 'volume', path: '/volume/:id', component: Volume},
		{name: 'bookmarks', path: '/bookmarks', component: Bookmarks},
		{name: 'history', path: '/history', component: History}
	]

});

export default router;