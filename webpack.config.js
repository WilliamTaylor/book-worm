var path = require('path');

let webpack = require('webpack');

module.exports = {
	entry: [
		'./app/index.js'
	],
	devtool: 'source-map',
	target: 'web',
	output: {
		filename: 'bundle.js',
		path: path.resolve(__dirname, 'dist'),
		publicPath: "/dist/"
	},
	resolve: {
		alias: {
			vue: 'vue/dist/vue.js'
		}
	},

	devServer: {
		contentBase: path.join(__dirname, "/"),
		compress: true,
		port: 9251
	},

	module: {
		rules: [

			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
				options: {
					retainLines: true,
					presets: ['es2015'],
					plugins: ['transform-runtime']
				}
			},
			{
				test: /\.vue$/,
				loader: 'vue-loader',
				options: {
					loaders: {
						js: 'babel-loader'
					}
				}
			},
			{
				test: /\.(?:jpg|gif|png)$/,
				loader: 'file'
			}

		]
	},

	plugins: [
		new webpack.optimize.OccurrenceOrderPlugin
	]

};