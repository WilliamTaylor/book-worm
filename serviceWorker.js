const cacheName = 'v2';

const cachedURLS = [
	'./',
	'./index.html',
	'./manifest.json',
	'./dist/bundle.js',
	'./styles.css',
	'./media/logo.svg',
	'./media/icons/favicon-16x16.png',
	'https://fonts.googleapis.com/icon?family=Material+Icons',
	'https://fonts.googleapis.com/css?family=Open+Sans:400,700'

];

self.oninstall = function (e) {
	console.log('Installing Service worker');

	e.waitUntil(
		caches.open(cacheName)
			.then(function (cache) {
				return cache.addAll(cachedURLS);
			})
			.then(function() {
				return self.skipWaiting();
			})
	);
};

self.onfetch = function (e) {

	e.respondWith(
		caches.match(e.request).then(function (response) {
			if (response) return response;
			return fetch(e.request)
		})
	);
};