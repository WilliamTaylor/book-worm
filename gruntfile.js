module.exports = function(grunt) {

	grunt.initConfig({

		sass: {
			options: {
				sourceMap: true
			},

			dist: {
				files: {
					'styles.css': 'styles/includes.scss'
				}
			}
		},

		watch: {
			css: {
				files: 'styles/**/*.scss',
				tasks: ['sass']
			}
		}

	});

	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('default', ['sass']);
	grunt.registerTask('watchsass', ['sass', 'watch']);

};

